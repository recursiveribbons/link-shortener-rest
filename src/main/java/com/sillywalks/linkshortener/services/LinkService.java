package com.sillywalks.linkshortener.services;

import com.sillywalks.linkshortener.model.LinkRepository;
import com.sillywalks.linkshortener.types.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LinkService {

    private final LinkRepository linkRepository;

    @Autowired
    public LinkService(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    public List<Link> getLinks() {
        List<Link> links = new ArrayList<>();
        linkRepository.findAll().forEach(links::add);
        return links;
    }

    public Optional<Link> getLink(String linkId) {
        return linkRepository.findById(linkId);
    }

    public boolean linkExists(String linkId) {
        return linkRepository.existsById(linkId);
    }

    public Link newLink(Link link) {
        return linkRepository.save(link);
    }

    public Link editLink(Link link) {
        linkRepository.deleteById(link.getLinkId());
        return linkRepository.save(link);
    }

    public void deleteLink(String linkId) {
        linkRepository.deleteById(linkId);
    }
}
