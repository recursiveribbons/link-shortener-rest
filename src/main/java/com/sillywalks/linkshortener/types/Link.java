package com.sillywalks.linkshortener.types;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Link {
    @Id
    private String linkId;

    private String linkURL;

    public Link() {}

    public Link(String linkId, String linkURL) {
        this.linkId = linkId.toLowerCase();
        this.linkURL = linkURL.toLowerCase();
    }

    public Link(String linkId, LinkOptions linkOptions) {
        this(linkId, linkOptions.getLinkURL());
    }

    public String getLinkId() {
        return linkId;
    }

    public String getLinkURL() {
        return linkURL;
    }
}
