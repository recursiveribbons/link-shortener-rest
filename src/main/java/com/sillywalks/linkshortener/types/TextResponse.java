package com.sillywalks.linkshortener.types;

class TextResponse {
    private String message;

    public TextResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
