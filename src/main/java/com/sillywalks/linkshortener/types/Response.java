package com.sillywalks.linkshortener.types;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response extends ResponseEntity<TextResponse> {

    public Response(String message, HttpStatus status) {
        super(new TextResponse(message), status);
    }
}
