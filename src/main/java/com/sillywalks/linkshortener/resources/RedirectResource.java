package com.sillywalks.linkshortener.resources;

import com.sillywalks.linkshortener.exceptions.LinkNotFoundException;
import com.sillywalks.linkshortener.services.LinkService;
import com.sillywalks.linkshortener.types.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/")
public class RedirectResource {
    private final LinkService linkService;

    @Autowired
    public RedirectResource(LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping("{linkId:[_a-zA-Z0-9]+}")
    public ResponseEntity getRedirect(@PathVariable String linkId) {
        Link link = linkService.getLink(linkId).orElseThrow(LinkNotFoundException::new);
        HttpHeaders headers = new HttpHeaders();
        String url = link.getLinkURL();
        if(!url.startsWith("http")) {
            url = "http://" + url;
        }
        headers.add("Location", url);
        return new ResponseEntity<>(headers, HttpStatus.PERMANENT_REDIRECT);
    }
}
