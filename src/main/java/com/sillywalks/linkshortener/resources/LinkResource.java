package com.sillywalks.linkshortener.resources;

import com.sillywalks.linkshortener.exceptions.LinkNotFoundException;
import com.sillywalks.linkshortener.services.LinkService;
import com.sillywalks.linkshortener.types.Link;
import com.sillywalks.linkshortener.types.LinkOptions;
import com.sillywalks.linkshortener.types.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/links", produces="application/json")
public class LinkResource {
    private final LinkService linkService;

    @Autowired
    public LinkResource(LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping
    public List<Link> getLinks() {
        return linkService.getLinks();
    }

    @GetMapping("{linkId:[_a-zA-Z0-9]+}")
    public Link getLink(@PathVariable String linkId) {
        return linkService.getLink(linkId).orElseThrow(LinkNotFoundException::new);
    }

    @PostMapping
    public ResponseEntity newLink(@RequestBody Link link) {
        if(linkService.linkExists(link.getLinkId())) {
            return new Response("Link already exists", HttpStatus.BAD_REQUEST);
        } else {
            link = linkService.newLink(link);
            if(link == null) {
                return new Response("Database error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(link, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200", "http://localhost"})
    @PutMapping("{linkId:[_a-zA-Z0-9]+}")
    @PatchMapping("{linkId:[_a-zA-Z0-9]+}")
    public ResponseEntity editLink(@PathVariable("linkId") String linkId, @RequestBody LinkOptions linkOptions) {
        if(linkService.linkExists(linkId)) {
            Link link = new Link(linkId, linkOptions);
            link = linkService.editLink(link);
            if(link == null) {
                return new Response("Database error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(link, HttpStatus.OK);
        } else {
            return new Response("Link not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{linkId:[_a-zA-Z0-9]+}")
    public Response deleteLink(@PathVariable("linkId") String linkId) {
        if(linkService.linkExists(linkId)) {
            linkService.deleteLink(linkId);
            return new Response("Link deleted", HttpStatus.OK);
        } else {
            return new Response("Link not found", HttpStatus.NOT_FOUND);
        }
    }
}
