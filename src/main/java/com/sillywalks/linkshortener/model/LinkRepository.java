package com.sillywalks.linkshortener.model;

import com.sillywalks.linkshortener.types.Link;
import org.springframework.data.repository.CrudRepository;

public interface LinkRepository extends CrudRepository<Link, String> {
}
