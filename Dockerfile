FROM openjdk:8-jre-alpine
COPY build/libs/*.jar /opt/link-shortener/lib/app.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-Dspring.datasource.url=jdbc:hsqldb:file:/opt/link-shortener/db/", "-jar", "/opt/link-shortener/lib/app.jar"]
VOLUME /opt/link-shortener/db/
EXPOSE 8080